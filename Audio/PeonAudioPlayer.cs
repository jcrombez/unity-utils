﻿using UnityEngine;
using System.Collections;

public class PeonAudioPlayer : MonoBehaviour
{
    public AudioClip[] footSteps;

    public void PlayFootStep ()
    {
        if (footSteps != null) {
            AudioClipPlayer.PlayRandom(footSteps, transform.position);
        }
    }
}
