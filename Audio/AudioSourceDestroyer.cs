﻿using UnityEngine;

public class AudioSourceDestroyer : MonoBehaviour
{
    public AudioSource audioSource;

	void Update ()
    {
	    if (audioSource == null) {
            return;
        }

        if (audioSource.isPlaying) {
            return;
        }

        if (!audioSource.enabled) {
            Debug.LogWarning("AudioSourceDestroyer : destroying disabled audio source " + audioSource.name);
        }

        Destroy(audioSource.gameObject);
    }
}
