﻿using UnityEngine;

public class AudioClipPlayer
{
    public static void PlayRandom (AudioClip[] audioClips, Vector3 position, float volume = 1.0f)
    {
        if (audioClips.Length > 0) {
            int r = Random.Range(0, audioClips.Length);
            AudioClip audioClip = audioClips[r];
            AudioSource.PlayClipAtPoint(audioClip, position, volume);
        }
    }

    public static void Play (AudioEvent audioEvent, AudioSource existingAudioSource)
    {
        AudioSource audioSource = CreateAudioSourceFrom(existingAudioSource);

        audioEvent.Play(audioSource);
    }

    public static void Play (AudioEvent audioEvent, Vector3 position)
    {
        audioEvent.Play(CreateAudioSource(position, audioEvent.name));
    }

    public static void Play (AudioClip audioClip, AudioSource existingAudioSource)
    {
        AudioSource audioSource = CreateAudioSourceFrom(existingAudioSource);

        audioSource.clip = audioClip;
        audioSource.Play();
    }

    public static void Play (AudioClip audioClip, Vector3 position)
    {
        AudioSource audioSource = CreateAudioSource(position, audioClip.name);

        audioSource.clip = audioClip;
        audioSource.Play();
    }

    private static AudioSource CreateAudioSource (Vector3 position, string name = null)
    {
        GameObject gameObject = new GameObject();
        gameObject.name = "EphemeralAudioSource";

        if (name != null) {
            gameObject.name += "#" + name;
        }

        gameObject.transform.position = position;

        AudioSource audioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        audioSource.spatialBlend = 1;

        AudioSourceDestroyer audioSourceDestroyer = gameObject.AddComponent(typeof(AudioSourceDestroyer)) as AudioSourceDestroyer;
        audioSourceDestroyer.audioSource = audioSource;

        return audioSource;
    }

    private static AudioSource CreateAudioSourceFrom (AudioSource from)
    {
        GameObject gameObject = new GameObject();
        gameObject.name = "EphemeralAudioSource@" + from.name;
        gameObject.transform.position = from.transform.position;

        AudioSource audioSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        audioSource.volume = from.volume;
        audioSource.pitch = from.pitch;
        audioSource.minDistance = from.minDistance;
        audioSource.maxDistance = from.maxDistance;
        audioSource.rolloffMode = from.rolloffMode;
        audioSource.spatialBlend = from.spatialBlend;

        AudioSourceDestroyer audioSourceDestroyer = gameObject.AddComponent(typeof(AudioSourceDestroyer)) as AudioSourceDestroyer;
        audioSourceDestroyer.audioSource = audioSource;

        return audioSource;
    }
}
