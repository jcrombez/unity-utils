﻿using UnityEngine;

public class SphereGizmo : MonoBehaviour
{
    public Color color = Color.yellow;

    void OnDrawGizmosSelected ()
    {
        Gizmos.color = color;
        Gizmos.DrawSphere(transform.position, .1f);
    }
}
