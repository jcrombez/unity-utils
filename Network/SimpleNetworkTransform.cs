﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class SimpleNetworkTransform : NetworkBehaviour
{
    public float lerpFactor = 0f;

    // Ces variables SyncVar sont misent à jour par le propriétaire de l'objet par l'intermédiaire du serveur (via CmdSetLatestPosition)
    // et comme c'est une SyncVar, les modifications sont répercutés du serveurs vers tous les clients
    [SyncVar]
    public Vector3 latestPosition;
    public float positionDistanceTolerence = 0.001f;

    // Idem
    [SyncVar]
    public Quaternion latestRotation;
    public float rotationDistanceTolerence = 0.001f;

    [SyncVar]
    public Transform latestParent = null;

    void Start ()
    {
        // TODO : euh ?...
        if (!isServer && !isClient) {
            Debug.LogError("WUUUUT?!");
            enabled = false;
        }
    }

    private void Update ()
    {
        // Si je suis pas propriétaire de l'objet
        if (!hasAuthority) {
            InterpolateTransform();
        }
    }

    void FixedUpdate ()
    {
        // Si je suis propriétaire de l'objet
        if (hasAuthority) {
            SetLatestTransform();
        }
    }

    // Cette méthode est réservée au joueur ayant l'autorité sur l'objet
    // TODO : limiter l'envois de commande au serveur en fonction d'un paramètre d'envois par seconde
    private void SetLatestTransform ()
    {
        // On pourrait grouper le changement de parent avec le reste pour optimiser, mais ça n'arrive pas souvent alors bon...
        if (HasChangedParent()) {
            //Debug.Log("(authority) Object changed parent");

            NetworkIdentity parent = transform.parent != null ? transform.parent.GetComponent<NetworkIdentity>() : null;

            CmdSetParent(parent);
        }

        // Si l'objet a changé de position et de rotation depuis le dernier FixedUpdate,
        // on met à jour les deux dans le même RPC pour optimiser.
        if (HasChangedPosition() && HasChangedRotation()) {
            // Le propriétaire demande au serveur de mettre à jour latestPosition/latestRotation avec la position/rotation actuelle
            CmdSetLatestTransform(
                transform.localPosition,
                transform.localRotation
            );

            return;
        }

        if (HasChangedPosition()) {
            CmdSetLatestPosition(transform.localPosition);
            return;
        }

        if (HasChangedRotation()) {
            CmdSetLatestRotation(transform.localRotation);
            return;
        }
    }

    private bool HasChangedParent ()
    {
        return latestParent != transform.parent;
    }

    private bool HasChangedPosition ()
    {
        float positionDistance = Vector3.Distance(transform.localPosition, latestPosition);

        return positionDistance > positionDistanceTolerence;
    }

    private bool HasChangedRotation ()
    {
        float rotationDistance = Vector3.Distance(transform.localEulerAngles, latestRotation.eulerAngles);

        return rotationDistance > rotationDistanceTolerence;
    }

    // --- Commands

    [Command(channel = Channels.DefaultUnreliable)]
    private void CmdSetLatestPosition (Vector3 position)
    {
        latestPosition = position;
    }

    [Command(channel = Channels.DefaultUnreliable)]
    private void CmdSetLatestRotation (Quaternion rotation)
    {
        latestRotation = rotation;
    }

    [Command(channel = Channels.DefaultUnreliable)]
    private void CmdSetLatestTransform (Vector3 position, Quaternion rotation)
    {
        latestPosition = position;
        latestRotation = rotation;
    }

    [Command(channel = Channels.DefaultReliable)]
    private void CmdSetParent (NetworkIdentity parent)
    {
        latestParent = parent != null ? parent.transform : null;
    }

    // --- Client side interpolation

    // Mise à jour de la position/rotation en fonction de ce que le serveur m'a envoyé dans latestPosition/latestRotation,
    // tout en appliquant un Lerp pour que ça soit pas saccadé par l'interval d'envois de nouvelle position.
    // TODO : prendre en compte le lag : Networking.NetworkClient.GetRTT
    private void InterpolateTransform ()
    {
        if (HasChangedParent()) {
            //Debug.Log("(client) Object changed parent");
            transform.parent = latestParent;
        }

        // TODO : n'interpoler que ce qui a changé ?

        transform.localPosition = Vector3.Lerp(
            transform.localPosition,
            latestPosition,
            lerpFactor * Time.deltaTime
        );

        transform.localRotation = Quaternion.Lerp(
            transform.localRotation,
            latestRotation,
            lerpFactor * Time.deltaTime
        );
    }

    // Idem LerpTransform mais uniquement sur la position
    // TODO : prendre en compte le lag : Networking.NetworkClient.GetRTT
    private void LerpPosition ()
    {
        float distance = Vector3.Distance(transform.position, latestPosition);

        if (distance == 0) {
            return;
        }

        if (lerpFactor <= 0) {
            Debug.LogError("lerpFactor must be > 0");
        }

        transform.position = Vector3.Lerp(
            transform.position,
            latestPosition,
            lerpFactor * Time.deltaTime
        );
    }

    // Idem LerpTransform mais uniquement sur la rotation
    // TODO : prendre en compte le lag : Networking.NetworkClient.GetRTT
    private void LerpRotation ()
    {
        float distance = Vector3.Distance(transform.localEulerAngles, latestPosition);

        if (distance == 0) {
            return;
        }

        if (lerpFactor <= 0) {
            Debug.LogError("lerpFactor must be > 0");
        }

        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            latestRotation,
            lerpFactor * Time.deltaTime
        );
    }
}
