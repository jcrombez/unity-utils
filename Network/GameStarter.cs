﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class GameStarter : MonoBehaviour
{
    [SerializeField]
    private ServerManager serverManager;

    public bool forceCreate = false;

	void Start ()
    {
        if (forceCreate) {
            serverManager.CreateServer();
            return;
        }

        serverManager.GetServers(OnGetServers);
    }

    // TODO : simplifier la signature
    private void OnGetServers (bool success, string extendedInfo, List<MatchInfoSnapshot> responseData)
    {
        if (!success) {
            Debug.LogError("GetServers failed");
            return;
        }

        if (!serverManager.HasServers()) {
            Debug.Log("No server found");

            serverManager.CreateServer();
            return;
        }

        serverManager.JoinFirstServer(responseData);
    }

    void OnApplicationQuit ()
    {
        serverManager.StopServer();
    }
}
