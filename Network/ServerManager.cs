﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using System.Collections.Generic;

public class ServerManager : MonoBehaviour
{
    private MatchInfo currentMatch;

    //private NetworkManager networkManager;
    private NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> OnGetServers;

    void Awake ()
    {
        //networkManager = NetworkManager.singleton;
        NetworkManager.singleton.StartMatchMaker();
    }

    public List<MatchInfoSnapshot> Servers
    {
        get { return NetworkManager.singleton.matches; }
    }

    public bool HasServers ()
    {
        return NetworkManager.singleton.matches.Count > 0;
    }

    public void GetServers (NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> callback = null)
    {
        OnGetServers += callback;

        NetworkManager.singleton.matchMaker.ListMatches(
            startPageNumber: 0,
            resultPageSize: 10,
            matchNameFilter: "",
            filterOutPrivateMatchesFromResults: false,
            eloScoreTarget: 0,
            requestDomain: 0,
            //callback: callback
            callback: OnMatchList
            //callback: NetworkManager.singleton.OnMatchList
        );
    }

    private void OnMatchList (bool success, string extendedInfo, List<MatchInfoSnapshot> responseData)
    {
        NetworkManager.singleton.OnMatchList(success, extendedInfo, responseData);

        DebugMatchInfoSnapshots(responseData);

        if (OnGetServers != null) {
            OnGetServers(success, extendedInfo, responseData);
        }
    }

    public void CreateServer (NetworkMatch.DataResponseDelegate<MatchInfo> callback = null)
    {
        NetworkManager.singleton.matchName = "test " + System.DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss");

        bool isPublic = true;
        string matchPassword = "";

        NetworkManager.singleton.matchMaker.CreateMatch(
            NetworkManager.singleton.matchName,
            NetworkManager.singleton.matchSize,
            isPublic,
            matchPassword,
            publicClientAddress: "",
            privateClientAddress: "",
            eloScoreForMatch: 0,
            requestDomain: 0,
            //callback: callback
            callback: OnMatchCreate
            //callback: networkManager.OnMatchCreate
        );
    }

    private void OnMatchCreate (bool success, string extendedInfo, MatchInfo matchInfo)
    {
        currentMatch = matchInfo;

        NetworkManager.singleton.OnMatchCreate(success, extendedInfo, matchInfo);
    }

    public void JoinServer (NetworkID networkId, string password = "")
    {
        NetworkManager.singleton.matchMaker.JoinMatch(
            networkId,
            password,
            publicClientAddress: "",
            privateClientAddress: "",
            eloScoreForClient: 0,
            requestDomain: 0,
            callback: OnMatchJoined
        );
    }

    public void OnMatchJoined (bool success, string extendedInfo, MatchInfo matchInfo)
    {
        currentMatch = matchInfo;

        NetworkManager.singleton.OnMatchJoined(success, extendedInfo, matchInfo);
    }

    public void JoinFirstServer (List<MatchInfoSnapshot> responseData)
    {
        MatchInfoSnapshot matchInfo = FindFirstServer(responseData);

        if (matchInfo == null) {
            return;
        }

        JoinServer(matchInfo.networkId);
    }

    private MatchInfoSnapshot FindFirstServer (List<MatchInfoSnapshot> responseData)
    {
        responseData.Reverse();

        if (responseData.Count == 0) {
            Debug.LogError("Can't join first found server, the list is empty");
            return null;
        }

        responseData.Reverse();

        foreach (MatchInfoSnapshot matchInfo in responseData) {
            if (IsServerFull(matchInfo)) {
                Debug.Log("Can't join match #" + matchInfo.networkId + " : full (" + matchInfo.currentSize + "/" + matchInfo.maxSize + ")");
                continue;
            }

            if (IsServerEmpty(matchInfo)) {
                Debug.Log("Can't join match #" + matchInfo.networkId + " : empty (" + matchInfo.currentSize + "/" + matchInfo.maxSize + ")");
                continue;
            }

            Debug.Log("Joining match #" + matchInfo.networkId + " " + matchInfo.name + " (" + matchInfo.currentSize + "/" + matchInfo.maxSize + ")");

            return matchInfo;
        }

        return null;
    }

    private bool IsServerFull(MatchInfoSnapshot matchInfo)
    {
        return matchInfo.currentSize >= matchInfo.maxSize;
    }

    private bool IsServerEmpty(MatchInfoSnapshot matchInfo)
    {
        return matchInfo.currentSize == 0;
    }

    public void StopServer ()
    {
        // marche pô
        //// Hide the match to avoid joining stopped server
        //NetworkManager.singleton.matchMaker.SetMatchAttributes(
        //    networkId: createdMatch.networkId,
        //    isListed: false, 
        //    requestDomain: createdMatch.domain,
        //    callback: NetworkManager.singleton.OnSetMatchAttributes
        //);

        //NetworkManager.singleton.matchMaker.DestroyMatch(netId, 0);

        NetworkManager.singleton.StopHost();
    }

    private void OnApplicationQuit ()
    {
        // marche pô
        //NetworkManager.singleton.matchMaker.DropConnection(
        //    currentMatch.networkId, 
        //    NodeID.Invalid, 
        //    0, 
        //    (bool success, string extendedInfo) => Debug.Log("MatchConnectionDropped => " + success.ToString())
        //);

        // marche pô
        //NetworkManager.singleton.matchMaker.DestroyMatch(
        //    currentMatch.networkId, 
        //    0, 
        //    (bool success, string extendedInfo) => Debug.Log("OnDestroyMatch => " + success.ToString())
        //);
    }

    // --- Debug

    public static void DebugMatchInfoSnapshots (List<MatchInfoSnapshot> responseData)
    {
        foreach (MatchInfoSnapshot info in responseData) {
            Debug.Log("Server #" + info.networkId + " : " + info.name + " (" + info.currentSize + "/" + info.maxSize + ")");
        }
    }
}
