﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleNetworkSpawner : NetworkBehaviour
{
    public GameObject prefab;

    public void Spawn ()
    {
        GameObject go = Instantiate(prefab, transform.position, transform.rotation);
        NetworkServer.Spawn(go);
    }
}
