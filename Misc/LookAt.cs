﻿using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform target;

	void Update ()
    {
        transform.LookAt(target);
	}

    public void SetTarget (Transform target)
    {
        this.target = target;
    }
}
