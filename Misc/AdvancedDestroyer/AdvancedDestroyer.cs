﻿using UnityEngine;
using System.Collections;

public class AdvancedDestroyer : MonoBehaviour
{
    static public void FadeDestroy (GameObject gameObject, float time, float fadeDelay = 0)
    {
        FadeObjectInOut fader = gameObject.AddComponent<FadeObjectInOut>() as FadeObjectInOut;

        fader.FadeOut(time - fadeDelay, fadeDelay);

        Destroy(gameObject, time);
    }
}
