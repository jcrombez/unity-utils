﻿using UnityEngine;

public class EditorOnly : MonoBehaviour
{
    void OnEnable ()
    {
        if (!Application.isEditor) {
            Destroy(gameObject);
        }
    }
}
