﻿using UnityEngine;

public class PrefabPlaceholder : MonoBehaviour
{
    public GameObject prefab;
    private GameObject instance;

    public virtual void Awake ()
    {
        //GameObject instance = Instantiate(prefab, transform.position, transform.rotation, transform.parent) as GameObject;
    }

	public virtual void Start ()
    {
        Destroy(transform.gameObject);
	}
}
