﻿using UnityEngine;

public class ParticleSystemPlayer
{
    public static void PlayAt (GameObject visualEffect, Vector3 position, Quaternion rotation)
    {
        float destroyAfter = 0;

        foreach (ParticleSystem particleSystem in ComponentFinder.FindAll<ParticleSystem>(visualEffect)) {
            if (particleSystem.main.duration > destroyAfter) {
                destroyAfter = particleSystem.main.duration;
            }
        }

        GameObject.Destroy(
            GameObject.Instantiate(
                visualEffect, 
                position, 
                rotation
            ),
            destroyAfter + 1
        );
    }

    public static void PlayAt (GameObject visualEffect, Transform transform)
    {
        PlayAt(visualEffect, transform.position, transform.rotation);
    }

    public static void PlayAt (ParticleSystem particleSystem, Transform transform)
    {
        PlayAt(particleSystem, transform.position, transform.rotation);
    }

    public static void PlayAt (ParticleSystem particleSystem, Vector3 position, Quaternion rotation)
    {
        PlayAt(particleSystem.gameObject, position, rotation);
    }
}
