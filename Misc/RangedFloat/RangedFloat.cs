﻿using System;

[Serializable]
public struct RangedFloat
{
    public float minValue;
    public float maxValue;

    public static RangedFloat OneConstant = new RangedFloat(1f, 1f);

    public RangedFloat (float minValue = 0f, float maxValue = 1f)
    {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public static RangedFloat Constant (float value)
    {
        return new RangedFloat(value, value);
    }
}