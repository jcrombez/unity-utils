﻿using UnityEngine;
using System.Collections.Generic;

public class ComponentFinder
{
    public static List<T> FindAll<T> (GameObject root)
    {
        var components = new List<T>();

        T rootComponent = root.GetComponent<T>();

        if (rootComponent != null && !rootComponent.Equals(null)) {
            components.Add(rootComponent);
        }

        foreach (var component in root.GetComponentsInChildren<T>()) {
            if (!components.Contains(component)) { // il semblerait que le root component soit retourné dans GetComponentsInChildren...
                components.Add(component);
            }
        }

        return components;
    }
}
